from yaml import safe_load,safe_dump
from typing import Union

SETTING={}

def loadSettings() -> bool:
    """
    Loads the settings from a file.
    """
    try:
        with open("settings.yaml", "r", encoding="utf-8") as f:
            global SETTING
            SETTING=safe_load(f) 
            return True
    except FileNotFoundError:
            return False
    
def saveSettings(settings:dict) ->bool:
     """
     Saves the settings to a file.
     """
     try:
        with open("settings.yaml", "w", encoding="utf-8") as f:
            safe_dump(settings, f)
            return True
     except FileNotFoundError:
            return False

try:
     loadSettings()
except Exception as e:
     print(e)