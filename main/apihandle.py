from httpx import Client, ConnectError, ConnectTimeout
import urllib.request
from wx import Image
from io import BytesIO
from datetime import datetime
from json import dumps
from settings import SETTING

try:
    WEB_URL = SETTING["server"]["mainURL"]
except Exception:
    WEB_URL = "http://127.0.0.1"
client = Client(headers={"User-Agent": "SavingGetter V1.0Alpha"}, base_url=WEB_URL)


def webTest() -> bool:
    # 测试网络是否正常
    try:
        response = client.get("/client/login/")
        if response.status_code == 200:
            return True
        else:
            return False
    except (ConnectTimeout, ConnectError):
        return False


def login(username: str, passwd: str):
    # 登录,注意登录成功后返回False,登录失败后返回错误信息
    response = client.post(
        "/client/login/",
        content=dumps({"username": username, "password": passwd}),
        headers={"Content-Type": "application/json"},
    )
    if response.status_code == 200:
        if response.json()["status"] == "success":
            return False
        else:
            return response.json()["message"]
    else:
        return f"发生网络错误，错误代码:{str(response.status_code)}\n错误信息:{response.text}"


def getActivity():
    # 获取活动信息
    response = client.get("/client/activity/")
    if response.status_code == 200:
        if response.json()["status"] == "error":
            client.get("/client/login/")
        return response.json()
    else:
        return f"发生网络错误，错误代码:{str(response.status_code)}\n错误信息:{response.text}"


def get_saving():
    try:
        response = client.get("/client/saving/")
        if response.status_code == 200:
            if (
                response.headers.get("content-type") == "application/json"
                and response.json().get("status") == "error"
            ):
                return response.json()["message"]
            else:
                with open("saving.zip", "wb") as fi:
                    fi.write(response.content)
                return True
        else:
            return f"发生网络错误，错误代码:{str(response.status_code)}\n错误信息:{response.text}"

    except Exception as e:
        return f"请求出错: {e}"
    finally:
        client.get("/client/login/")


def photoFileGet(url: str):
    image_data = BytesIO(urllib.request.urlopen(url).read())
    image = Image(image_data)
    return image


def compare_to_datetime(target_datetime_tuple: tuple):
    # 比较时间是否在目标时间范围内,输入为(开始时间,结束时间)
    start_datetime = datetime.strptime(target_datetime_tuple[0], "%Y-%m-%dT%H:%M:%S")
    end_datetime = datetime.strptime(target_datetime_tuple[1], "%Y-%m-%dT%H:%M:%S")
    current_datetime = datetime.now()
    if current_datetime >= start_datetime and current_datetime <= end_datetime:
        return True
    else:
        return False
